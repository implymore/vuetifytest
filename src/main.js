import Vue from 'vue';
import App from './App.vue';
import vuetify from './plugins/vuetify';
import router from './router';
import Highcharts from "highcharts";
import HighchartsVue from 'highcharts-vue';
import stockInit from 'highcharts/modules/stock';
import Gantt from 'highcharts/modules/gantt';

// import View from 'ol/View'
// import Map from 'ol/Map'
// import TileLayer from 'ol/layer/Tile'
// import OSM from 'ol/source/OSM'

// importing the OpenLayers stylesheet is required for having
// good looking buttons!
// import 'ol/ol.css'

//import data from './plugins/data';
//const d3 = require("d3");
import VueAgile from 'vue-agile';

// import moment from 'moment';

// Object.defineProperty(Vue.prototype, '$moment', { value: moment });




Vue.use(HighchartsVue);
Vue.use(VueAgile);
stockInit(Highcharts);
Gantt(Highcharts);

Vue.config.productionTip = false;

new Vue({
  vuetify,
  //data,
  router,
  render: h => h(App)
}).$mount('#app')
